﻿using Discord;
using Discord.WebSocket;
using System.Text;

namespace UnBot
{
    public class MotusCommand : CommandModule
    {
        private List<Motus> _motusQueue = new List<Motus>();
        public override SlashCommandProperties Command { get; } = new SlashCommandBuilder()
            .WithName("motus")
            .WithDescription("Motus !!")
            .AddOption(new SlashCommandOptionBuilder()
                            .WithName("start")
                            .WithDescription("Start a game")
                            .WithType(ApplicationCommandOptionType.SubCommand)
                            .AddOption("word", ApplicationCommandOptionType.String, "Word to guess", isRequired: true)
                            .AddOption("free-length", ApplicationCommandOptionType.Boolean, "Can the guess be any length", isRequired: true))
            .AddOption(new SlashCommandOptionBuilder()
                            .WithName("guess")
                            .WithDescription("Guess the word")
                            .WithType(ApplicationCommandOptionType.SubCommand)
                            .AddOption("guess", ApplicationCommandOptionType.String, "Guess", isRequired: true))
            .AddOption(new SlashCommandOptionBuilder()
                            .WithName("status")
                            .WithDescription("Current game status")
                            .WithType(ApplicationCommandOptionType.SubCommand))
            .AddOption(new SlashCommandOptionBuilder()
                            .WithName("queue")
                            .WithDescription("Displays the word queue")
                            .WithType(ApplicationCommandOptionType.SubCommand))
            .AddOption(new SlashCommandOptionBuilder()
                            .WithName("reveal")
                            .WithDescription("Reveal the current word (if it's yours)")
                            .WithType(ApplicationCommandOptionType.SubCommand))
            .Build();
  
        public MotusCommand(DiscordSocketClient client) : base(client) {}

        private async void StartFirstInQueue(ISocketMessageChannel channel)
        {
            var motus = _motusQueue.First();
            await channel.SendMessageAsync($"Starting Motus (word requested by {motus.Owner.Mention})");
            SendStatus(channel, motus);
        }
        private async void SendStatus(ISocketMessageChannel channel, Motus motus)
        {
            var sb = new StringBuilder();

            var preTreated = motus.FreeLength ? new string(motus.GuessedLetters).TrimEnd() : new string(motus.GuessedLetters);

            foreach (var c in preTreated)
            {
                sb.Append($"{Motus.EmojiForLetter(c, 0)} ");
            }

            if(preTreated.Length < motus.GuessedLetters.Length)
            {
                sb.Append("▶️");
            }

            await channel.SendMessageAsync(sb.ToString());
        }
        private async void Guess(SocketSlashCommand command, Motus motus, string guess)
        {
            var matches = motus.Guess(guess);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < guess.Length; i++)
            {
                sb.Append(Motus.EmojiForLetter(guess[i], i < matches.Length ? matches[i] : 0));
                sb.Append(" ");
            }
            if(guess.Length > motus.Word.Length)
            {
                sb.Append("◀️");
            }
            else if(guess.Length < motus.Word.Length)
            {
                sb.Append("▶️");
            }

            await command.RespondAsync(sb.ToString());

            if (motus.Word == guess)
            {
                _motusQueue.Remove(motus);
                if (_motusQueue.Count > 0)
                {
                    StartFirstInQueue(command.Channel);
                }
            }
        }

        public override async Task HandleCommand(SocketSlashCommand command)
        {
            if (command.Data.Name == "motus")
            {
                var sub = command.Data.Options.First().Name;
                Console.WriteLine($"{command.User.Username} used motus {sub}");
                switch (sub)
                {
                    case "start":
                        await HandleStartCommand(command);
                        break;
                    case "guess":
                        await HandleGuessCommand(command);
                        break;
                    case "status":
                        await HandleStatusCommand(command);
                        break;
                    case "reveal":
                        await HandleRevealCommand(command);
                        break;
                    case "queue":
                        await HandleQueueCommand(command);
                        break;
                }
            }
        }

        private async Task HandleStartCommand(SocketSlashCommand command)
        {
            var word = command.Data.Options.First().Options?.FirstOrDefault().Value.ToString().ToLower();
            var freeLength = (bool)command.Data.Options.First().Options?.LastOrDefault().Value;
            _motusQueue.Add(new Motus(word, freeLength, command.User));
            await command.RespondAsync($"Your word has been queued at position {_motusQueue.Count}.", ephemeral: true);
            if (_motusQueue.Count == 1)
            {
                StartFirstInQueue(command.Channel);
            }
        }
        private async Task HandleGuessCommand(SocketSlashCommand command)
        {
            var guess = command.Data.Options.First().Options?.FirstOrDefault().Value.ToString().ToLower();
            if(_motusQueue.Count > 0)
            {
                var motus = _motusQueue.First();
                if (guess.Length != motus.Word.Length && !motus.FreeLength)
                {
                    await command.RespondAsync("Guess isn't same length", ephemeral: true);
                }
                else
                {
                    Guess(command, motus, guess);
                }
            } 
            else
            {
                await command.RespondAsync("No game has started yet", ephemeral: true);
            }
        }
        private async Task HandleStatusCommand(SocketSlashCommand command)
        {
            if (_motusQueue.Count > 0)
            {
                var motus = _motusQueue.First();
                await command.RespondAsync("Showing status of current game", ephemeral: true);
                SendStatus(command.Channel, motus);
            }
            else
            {
                await command.RespondAsync("No game has started yet", ephemeral: true);
            }
        }
        private async Task HandleRevealCommand(SocketSlashCommand command)
        {
            if (_motusQueue.Count > 0)
            {
                var motus = _motusQueue.First();
                if (motus.Owner.Id == command.User.Id)
                {
                    Guess(command, motus, motus.Word);
                }
                else
                {
                    await command.RespondAsync("This word isn't yours", ephemeral: true);
                }
            }
            else
            {
                await command.RespondAsync("No game has started yet", ephemeral: true);
            }
        }
        private async Task HandleQueueCommand(SocketSlashCommand command)
        {
            var sb = new StringBuilder();
            for(int i = 1; i < Math.Min(_motusQueue.Count, 6); i++)
            {
                sb.Append($"{i}. {_motusQueue[i].Owner.Username}\n");
            }

            await command.RespondAsync(embeds: new Embed[]{
                new EmbedBuilder().WithAuthor(_client.CurrentUser).WithTitle("Words in the queue").WithDescription(sb.ToString()).Build() 
            });
        }
    }

    public class Motus
    {
        public static string[,] Emojis = new string[26, 3]
        {
            {"🇦", "<:aj:1034454150416703608>", "🅰"},
            {"🇧", "<:bj:1034454153583394917>", "🅱"},
            {"🇨", "<:cj:1034454156552966145>", "<:cr:1034454158419439675>"},
            {"🇩", "<:dj:1034454160030052392>", "<:dr:1034454161024098416>"},
            {"🇪", "<:ej:1034454162794094692>", "<:er:1034454164249526365>"},
            {"🇫", "<:fj:1034454165663010826>", "<:fr:1034454167063887912>"},
            {"🇬", "<:gj:1034454168141844491>", "<:gr:1034454169769218049>"},
            {"🇭", "<:hj:1034454171396620342>", "<:hr:1034454173359554610>"},
            {"🇮", "<:ij:1034454175519608832>", "<:ir:1034454177549652048>"},
            {"🇯", "<:jj:1034454179537764482>", "<:jr:1034454180926066710>"},
            {"🇰", "<:kj:1034454182616387644>", "<:kr:1034454184155676692>"},
            {"🇱", "<:lj:1034454186210885672>", "<:lr:1034454187813109780>"},
            {"🇲", "<:mj:1034454189889298494>", "<:mr:1034454191306977332>"},
            {"🇳", "<:nj:1034454193219584121>", "<:nr:1034454194742100009>"},
            {"🇴", "<:oj:1034454196642123867>", "<:or:1034454197833310269>"},
            {"🇵", "<:pj:1034454199779479702>", "<:pr:1034454200828047451>"},
            {"🇶", "<:qj:1034454203407540275>", "<:qr:1034454205408219246>"},
            {"🇷", "<:rj:1034454207329206352>", "<:rr:1034454209329889311>"},
            {"🇸", "<:sj:1034454210885980212>", "<:sr:1034454212366573669>"},
            {"🇹", "<:tj:1034454214358863912>", "<:tr:1034454215881396264>"},
            {"🇺", "<:uj:1034454217370370158>", "<:ur:1034454219014553620>"},
            {"🇻", "<:vj:1034454220528701490>", "<:vr:1034454222202220585>"},
            {"🇼", "<:wj:1034454223821222030>", "<:wr:1034454225859661944>"},
            {"🇽", "<:xj:1034454227495436370>", "<:xr:1034454229403848764>"},
            {"🇾", "<:yj:1034454231601664050>", "<:yr:1034454233770115082>"},
            {"🇿", "<:zj:1034547867735572632>", "<:zr:1034547869115486280>"}
        };

        public SocketUser Owner { get; }
        public string Word { get; }
        public char[] GuessedLetters { get; }
        public bool FreeLength { get; }
        
        public Motus(string word, bool freeLength, SocketUser owner)
        {
            Word = word;
            FreeLength = freeLength;
            GuessedLetters = new char[word.Length];
            GuessedLetters[0] = word[0];

            for(int i = 1; i < word.Length; i++)
            {
                GuessedLetters[i] = ' ';
            }

            Owner = owner;
        }

        public static string EmojiForLetter(char letter, int position) => letter >= 'a' && letter <= 'z' ? Emojis[letter - 'a', position] : "\U0001f7e6";
        public int[] Guess(string guess)
        {
            var letters = Word.ToList();
            var matches = new int[guess.Length];

            for (int i = 0; i < guess.Length; i++)
            {
                if (i < Word.Length && Word[i] == guess[i])
                {
                    matches[i] = 2;
                    GuessedLetters[i] = Word[i];
                    letters.Remove(guess[i]);
                }
                else
                {
                    matches[i] = Word.Contains(guess[i]) ? 1 : 0;
                }
            }

            for (int i = 0; i < guess.Length; i++)
            {
                if (matches[i] == 1)
                {
                    matches[i] = letters.Remove(guess[i]) ? 1 : 0;
                }
            }

            return matches;
        }
    }
}
