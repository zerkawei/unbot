﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using UnBot;
using Discord.Net;
using Newtonsoft.Json;
using System.Reflection.Metadata;

class Program
{
    // Program entry point
    static Task Main(string[] args) => new Program().MainAsync();

    private readonly DiscordSocketClient _client;
    private readonly CommandModule[] _modules;

    private Program()
    {
        _client = new DiscordSocketClient(new DiscordSocketConfig
        {
            LogLevel = LogSeverity.Info,
        });

        _modules = new CommandModule[]
        {
            new MotusCommand(_client)
        };

        _client.Log += Log;
        _client.Ready += InitCommands;
    }

    private static Task Log(LogMessage message)
    {
        switch (message.Severity)
        {
            case LogSeverity.Critical:
            case LogSeverity.Error:
                Console.ForegroundColor = ConsoleColor.Red;
                break;
            case LogSeverity.Warning:
                Console.ForegroundColor = ConsoleColor.Yellow;
                break;
            case LogSeverity.Info:
                Console.ForegroundColor = ConsoleColor.White;
                break;
            case LogSeverity.Verbose:
            case LogSeverity.Debug:
                Console.ForegroundColor = ConsoleColor.DarkGray;
                break;
        }
        Console.WriteLine($"{DateTime.Now,-19} [{message.Severity,8}] {message.Source}: {message.Message} {message.Exception}");
        Console.ResetColor();
        return Task.CompletedTask;
    }
    private async Task MainAsync()
    {
        var token = Environment.GetEnvironmentVariable("UnBotToken");

        await _client.LoginAsync(TokenType.Bot, token);
        await _client.StartAsync();

        await Task.Delay(Timeout.Infinite);
    }
    private async Task InitCommands()
    {
        foreach(var module in _modules)
        {
            _client.SlashCommandExecuted += module.HandleCommand;
        }

        foreach (var guild in _client.Guilds)
        {
            try
            {
                await guild.DeleteApplicationCommandsAsync();
                foreach (var module in _modules)
                {
                    await guild.CreateApplicationCommandAsync(module.Command);
                }
            }
            catch (HttpException exception)
            {
                var json = JsonConvert.SerializeObject(exception.Errors, Formatting.Indented);
                Console.WriteLine(json);
            }
        }
    }
}
