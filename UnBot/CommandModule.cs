﻿using Discord;
using Discord.WebSocket;

namespace UnBot
{
    public abstract class CommandModule
    {
        protected DiscordSocketClient _client;

        public abstract SlashCommandProperties Command { get; }

        public CommandModule(DiscordSocketClient client)
        {
            _client = client;
        }

        public abstract Task HandleCommand(SocketSlashCommand command);    
    }
}
